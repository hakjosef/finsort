import yaml

from finsort.reporter import Reporter

if __name__ == '__main__':
    Reporter(yaml.load(open('rep.yaml', 'r'))).report()
