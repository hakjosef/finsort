import yaml
import json

from finsort.parser import CsvParser


class Reporter:

    def __init__(self, config: dict):
        self.config = config
        self.account = config['account']
        self.output_file = config['output_file']
        self.bank_logs = config['bank_logs']
        self.categories = config.get('categories')

    def parse_logs(self) -> list:
        data = []
        for bank_log in self.bank_logs:
            data.extend(CsvParser(yaml.load(open(bank_log['config_path']))).load(bank_log['log_path']))
        return data

    def report(self):
        I_VALUE = 0
        I_PLACE = 1
        I_FIRST_ACCOUNT = 2

        data = self.parse_logs()
        places = {}

        for item in data:
            if item[I_FIRST_ACCOUNT] == self.account and item[I_VALUE] < 0:
                if item[I_PLACE] not in places:
                    item_data = dict(size=item[I_VALUE],
                                     count=1)
                    places[item[I_PLACE]] = item_data
                else:
                    places[item[I_PLACE]]['size'] += item[I_VALUE]
                    places[item[I_PLACE]]['count'] += 1

        leafes = [dict(name=k,
                       size=round(-v['size'], 1),
                       description='%d x' % v['count']) for k, v in places.items()]

        for category in self.categories: category['children'] = []

        for leaf in leafes:
            assigned = False
            for category in self.categories:
                if self.is_member_of_category(category['items'], leaf):
                    category['children'].append(leaf)
                    assigned = True
            if not assigned:
                self.categories[-1]['children'].append(leaf)

        children = []
        for category in self.categories:
            if len(category['children']) > 0:
                children.append(dict(name=category['name'],
                                     children=category['children']))

        result = dict(name='Costs', description='Overall costs for account: %s.' % self.account,
                      children=children)

        with open(self.output_file, 'w') as out_f:
            json_result = json.dumps(result, indent=4)
            out_f.write(json_result)

    @staticmethod
    def is_member_of_category(cat_items: list, leaf: dict) -> bool:
        for cat_item in cat_items:
            if cat_item in leaf['name'].lower():
                print(cat_item, '-->', leaf['name'])
                return True
        return False
