

class CsvParser:

    def __init__(self, config: dict):
        self.config = config
        self.columns = config['columns']
        self.separator = config['separator']
        self.encoding = config['encoding']
        self.original_columns_count = config['original_columns_count']
        self.cell_parsers = []
        self.parse_next_line = self.parse_head
        self.data = []

    def parse_head(self, head: str):
        col_names = head.split(self.separator)
        print(col_names)
        for column in self.columns:
            column['i'] = col_names.index(column['label'])
            column['parser'] = CellParser(column)   # Find column index.
            print(column['i'], column['label'])
        self.parse_next_line = self.parse_line

    def parse_line(self, line: str):
        values = line.split(self.separator)
        line_data = []
        for column in self.columns:
            line_data.append(column['parser'].parse(values[column['i']]))
        self.data.append(line_data)

    def load(self, input_file):
        with open(input_file, 'r', encoding=self.encoding) as in_f:
            for line in in_f.readlines():
                if line.count(self.separator) == self.original_columns_count - 1:
                    line = line.replace('\n', '').replace('\"', '').replace('\'', '').replace(u'\ufeff', '')       # Remove next-line character.
                    self.parse_next_line(line)
        print('%d data lines loaded.' % len(self.data))
        for line in self.data:
            print(line)
        return self.data


class CellParser:

    def __init__(self, column: dict):
        self.value_type = column.get('value_type')
        self.date_format = column.get('date_format', 'DD.MM.YYYY')
        self.decimal_point = column.get('decimal_point', ',')

    def parse(self, str_val: str) -> object:
        if self.value_type == 'float':
            return self._parse_float(str_val)
        elif self.value_type == 'string':
            return str_val
        else:
            raise Exception('Unknown value type: %d!' % self.value_type)

    def _parse_float(self, str_val: str) -> float:
        str_val = str_val.replace(self.decimal_point, '.')
        val = float(str_val)
        return val
